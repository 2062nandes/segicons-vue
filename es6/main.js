(function(){  
  new Vue({
    el: '#segicons',
    data: {
      showmodal: false,
      menutemporal: false,
      formSubmitted: false,
      vue: {
        nombre: '',
        telefono: '',
        movil: '',
        ciudad: '',
        direccion: '',
        email: '',
        mensaje: '',
        envio: ''
      },
      mainStart: [
        {
          href: './',
          title: 'Inicio',
          icon: 'icon-home'
        },
        {
          href: 'nosotros.html',
          title: 'Nosotros',
          icon: 'icon-us'
        },
        {
          href: 'contactos.html',
          title: 'Contactos',
          icon: 'icon-contact'
        }
      ],
      mainMenu: [
        {
          id: 0, 
          href: 'seguridad-industrial.html',
          title: 'Seguridad Industrial',
          delay: '0.0s',
          menuItem:[
            {
              ref: 'botas-de-cuero',
              title: 'Botas de cuero con punta de metal'
            },
            {
              ref: 'botas-de-goma',
              title: 'Botas de goma'
            },
            {
              ref: 'botines-de-cuero-dielectricos',
              title: 'Botines de cuero dieléctrico'
            },
            {
              ref: 'cascos',
              title: 'Cascos'
            },
            {
              ref: 'overoles',
              title: 'Overoles'
            },
            {
              ref: 'chalecos',
              title: 'Chalecos'
            },
            {
              ref: 'lentes',
              title: 'Lentes'
            },
            {
              ref: 'guantes-de-todo-tipo',
              title: 'Guantes de todo tipo'
            },
            {
              ref: 'protectores',
              title: 'Protectores'
            },
            {
              ref: 'arneses-de-seguridad',
              title: 'Arneses de seguridad'
            },
            {
              ref: 'extintores',
              title: 'Extintores'
            },
            {
              ref: 'senaletica',
              title: 'Señalética'
            },
            {
              ref: 'conos-y-balizas',
              title: 'Conos y balizas'
            }
          ]
        },
        {
          id: 1,
          href: 'ropa-de-trabajo.html',
          title: 'Ropa de trabajo',
          delay: '0.1s',
          menuItem:[
            {
              ref: 'overoles',
              title: 'Overoles'
            },
            {
              ref: 'ponchillos-impermeables',
              title: 'Ponchillos impermeables'
            },
            {
              ref: 'chalecos',
              title: 'Chalecos'
            }            
          ]
        },
        {
          id: 2, 
          href: 'confecciones-en-general.html',
          title: 'Confecciones en general',
          delay: '0.2s',
          menuItem:[
            {
              ref: 'chamarras',
              title: 'Chamarras'
            },
            {
              ref: 'pantalones',
              title: 'Pantalones'
            },
            {
              ref: 'overoles',
              title: 'Overoles'
            },
            {
              ref: 'chalecos',
              title: 'Chalecos'
            },
            {
              ref: 'poleras',
              title: 'Poleras'
            },
            {
              ref: 'gorras',
              title: 'Gorras'
            },
            {
              ref: 'mochilas',
              title: 'Mochilas'
            },
            {
              ref: 'maletines',
              title: 'Maletines'
            },
            {
              ref: 'carpas',
              title: 'Carpas'
            },
            {
              ref: 'equipo-de-camping',
              title: 'Equipo de camping'
            }
          ]
        },
        {
          id: 3, 
          href: 'estructuras-metalicas.html',
          title: 'Estructuras metálicas',
          delay: '0.3s',
          menuItem:[
            {
              ref: 'tinglados',
              title: 'Tinglados'
            },
            {
              ref: 'cerchas',
              title: 'Cerchas'
            },
            {
              ref: 'cubiertas',
              title: 'Cubiertas'
            }
          ]
        },
        {
          id: 4, 
          href: 'construccion-de-campamentos.html',
          title: 'Construcción de campamentos',
          delay: '0.4s',
          menuItem:[
            {
              ref: 'dormitorios',
              title: 'Dormitorios'
            },
            {
              ref: 'oficinas',
              title: 'Oficinas'
            },
            {
              ref: 'baños-portatiles',
              title: 'Baños portátiles'
            }
          ]
        },
        {
          id: 5, 
          href: 'letreros-gigantografias.html',
          title: 'Letreros gigantografías',
          delay: '0.5s',
          menuItem:[
            {
              ref: 'letreros-luminosos',
              title: 'Letreros luminosos'
            },
            {
              ref: 'banners',
              title: 'Banners'
            },
            {
              ref: 'vallas',
              title: 'Vallas'
            },
            {
              ref: 'adhesivos',
              title: 'Adhesivos'
            },
            {
              ref: 'microperforados',
              title: 'Microperforados'
            }
          ]
        },
        {
          id: 6, 
          href: 'serigrafia.html',
          title: 'Serigrafía',
          delay: '0.6s',
          menuItem:[
            {
              ref: 'material-publicitario',
              title: 'Material publicitario'
            },
            {
              ref: 'estampados',
              title: 'Estampados'
            },
            {
              ref: 'bolsas',
              title: 'Bolsas'
            }
          ]
        },
        {
          id: 7,
          href: 'carpas-y-sombrillas.html',
          title: 'Carpas y sombrillas',
          delay: '0.7s',
          menuItem: [
            {
              ref: 'carpas-para-camping',
              title: 'Carpas para camping'
            },
            {
              ref: 'sombrillas',
              title: 'Sombrillas'
            }
          ]
        },
        {
          id: 8,
          href: 'articulos-publicitarios.html',
          title: 'Articulos publicitarios',
          delay: '0.8s',
          menuItem: [
            {
              ref: 'articulos',
              title: 'Articulos publicitarios'
            }
          ]
        }
      ],
      mainFooter: [
        {
          href: 'seguridad-industrial.html',
          title: 'Seguridad Industrial',
          thumb: 'images/thumbs/slide1.jpg'
        },
        {
          href: 'ropa-de-trabajo.html',
          title: 'Ropa de trabajo',
          thumb: 'images/thumbs/slide2.jpg'
        },
        {
          href: 'confecciones-en-general.html',
          title: 'Confecciones en general',
          thumb: 'images/thumbs/slide3.jpg'
        },
        {
          href: 'estructuras-metalicas.html',
          title: 'Estructuras metálicas',  
          thumb: 'images/thumbs/slide4.jpg'
        },
        {
          href: 'construccion-de-campamentos.html',
          title: 'Construcción de campamentos',
          thumb: 'images/thumbs/slide5.jpg'
        },
        {
          href: 'letreros-gigantografias.html',
          title: 'Letreros gigantografías', 
          thumb: 'images/thumbs/slide6.jpg'
        },
        {
          href: 'serigrafia.html',
          title: 'Serigrafia',
          thumb: 'images/thumbs/slide7.png'
        },
        {
          href: 'none',
          title: 'Señalización y señalética',
          thumb: 'images/thumbs/slide8.png'
        }
      ],
      gallery1:[
        {
          title: 'Botas de cuero con punta de metal',
          thumbs: [
            {
              thumb: 'images/thumbs/botas-de-cuero01.jpg',
              full: 'images/botas-de-cuero01.jpg'
            },
            {
              thumb: 'images/thumbs/botas-de-cuero02.jpg',
              full: 'images/botas-de-cuero02.jpg'
            },
            {
              thumb: 'images/thumbs/botas-de-cuero03.jpg',
              full: 'images/botas-de-cuero03.jpg'
            }
          ]
        },
        {
          title: 'Botas de goma',
          thumbs: [
            {
              thumb: 'images/thumbs/botas-de-goma01.jpg',
              full: 'images/botas-de-goma01.jpg'
            },
            {
              thumb: 'images/thumbs/botas-de-goma02.jpg',
              full: 'images/botas-de-goma02.jpg'
            },
            {
              thumb: 'images/thumbs/botas-de-goma03.jpg',
              full: 'images/botas-de-goma03.jpg'
            }
          ]
        },
        {
          title: 'Botines de cuero dieléctrico',
          thumbs: [
            {
              thumb: 'images/thumbs/botin01.jpg',
              full: 'images/botin01.jpg'
            },
            {
              thumb: 'images/thumbs/botin02.jpg',
              full: 'images/botin02.jpg'
            },
            {
              thumb: 'images/thumbs/botin03.jpg',
              full: 'images/botin03.jpg'
            }
          ]
        },
        {
          title: 'Cascos',
          thumbs: [
            {
              thumb: 'images/thumbs/cascos02.jpg',
              full: 'images/cascos02.jpg'
            },
            {
              thumb: 'images/thumbs/cascos01.jpg',
              full: 'images/cascos01.jpg'
            },
            {
              thumb: 'images/thumbs/cascos03.jpg',
              full: 'images/cascos03.jpg'
            }            
          ]
        },
        {
          title: 'Overoles',
          thumbs: [
            {
              thumb: 'images/thumbs/overol01.jpg',
              full: 'images/overol01.jpg'
            },
            {
              thumb: 'images/thumbs/overol02.jpg',
              full: 'images/overol02.jpg'
            },
            {
              thumb: 'images/thumbs/overol03.jpg',
              full: 'images/overol03.jpg'
            }
          ]
        },
        {
          title: 'Chalecos',
          thumbs: [
            {
              thumb: 'images/thumbs/chalecos01.jpg',
              full: 'images/chalecos01.jpg'
            },
            {
              thumb: 'images/thumbs/chalecos02.jpg',
              full: 'images/chalecos02.jpg'
            },
            {
              thumb: 'images/thumbs/chalecos03.jpg',
              full: 'images/chalecos03.jpg'
            }
          ]
        },
        {
          title: 'Lentes',
          thumbs: [
            {
              thumb: 'images/thumbs/lentes01.jpg',
              full: 'images/lentes01.jpg'
            },
            {
              thumb: 'images/thumbs/lentes02.jpg',
              full: 'images/lentes02.jpg'
            },
            {
              thumb: 'images/thumbs/lentes03.jpg',
              full: 'images/lentes03.jpg'
            }
          ]
        },
        {
          title: 'Guantes de todo tipo',
          thumbs: [
            {
              thumb: 'images/guantes01.jpg',
              full: 'images/guantes01.jpg'
            },
            {
              thumb: 'images/guantes02.jpg',
              full: 'images/guantes02.jpg'
            },
            {
              thumb: 'images/guantes03.jpg',
              full: 'images/guantes03.jpg'
            }
          ]
        },
        {
          title: 'Protectores',
          thumbs: [
            {
              thumb: 'images/thumbs/protectores01.jpg',
              full: 'images/protectores01.jpg'
            },
            {
              thumb: 'images/thumbs/protectores02.jpg',
              full: 'images/protectores02.jpg'
            },
            {
              thumb: 'images/thumbs/protectores03.jpg',
              full: 'images/protectores03.jpg'
            }
          ]
        },
        {
          title: 'Arneses de seguridad',
          thumbs: [
            {
              thumb: 'images/thumbs/arnes01.jpg',
              full: 'images/arnes01.jpg'
            },
            {
              thumb: 'images/thumbs/arnes02.jpg',
              full: 'images/arnes02.jpg'
            },
            {
              thumb: 'images/thumbs/arnes03.jpg',
              full: 'images/arnes03.jpg'
            }
          ]
        },
        {
          title: 'Extintores',
          thumbs: [
            {
              thumb: 'images/thumbs/extintor01.jpg',
              full: 'images/extintor01.jpg'
            },
            {
              thumb: 'images/thumbs/extintor02.jpg',
              full: 'images/extintor02.jpg'
            },
            {
              thumb: 'images/thumbs/extintor03.jpg',
              full: 'images/extintor03.jpg'
            }
          ]
        },
        {
          title: 'Señalética',
          thumbs: [
            {
              thumb: 'images/thumbs/señaletica01.jpg',
              full: 'images/señaletica01.jpg'
            },
            {
              thumb: 'images/thumbs/señaletica02.jpg',
              full: 'images/señaletica02.jpg'
            },
            {
              thumb: 'images/thumbs/slide8.png',
              full: 'images/slider/slide8.png'
            }
          ]
        },
        {
          title: 'Conos y balizas',
          thumbs: [
            {
              thumb: 'images/thumbs/conos-balizas01.jpg',
              full: 'images/conos-balizas01.jpg'
            },
            {
              thumb: 'images/thumbs/conos-balizas02.jpg',
              full: 'images/conos-balizas02.jpg'
            },
            {
              thumb: 'images/thumbs/conos-balizas03.jpg',
              full: 'images/conos-balizas03.jpg'
            }
          ]
        },
      ],
      gallery2: [
        {
          title: 'Overoles',
          thumbs: [
            {
              thumb: 'images/thumbs/overol01.jpg',
              full: 'images/overol01.jpg'
            },
            {
              thumb: 'images/thumbs/overol02.jpg',
              full: 'images/overol02.jpg'
            },
            {
              thumb: 'images/thumbs/overol03.jpg',
              full: 'images/overol03.jpg'
            }
          ]
        },
        {
          title: 'Ponchillos impermeables',
          thumbs: [
            {
              thumb: 'images/thumbs/ponchillos01.jpg',
              full: 'images/ponchillos01.jpg'
            },
            {
              thumb: 'images/thumbs/ponchillos02.jpg',
              full: 'images/ponchillos02.jpg'
            },
            {
              thumb: 'images/thumbs/ponchillos03.jpg',
              full: 'images/ponchillos03.jpg'
            }
          ]
        },
        {
          title: 'Chalecos',
          thumbs: [
            {
              thumb: 'images/thumbs/chalecos01.jpg',
              full: 'images/chalecos01.jpg'
            },
            {
              thumb: 'images/thumbs/chalecos02.jpg',
              full: 'images/chalecos02.jpg'
            },
            {
              thumb: 'images/thumbs/chalecos03.jpg',
              full: 'images/chalecos03.jpg'
            }
          ]
        },        
        {
          title: 'Equipo de camping',
          thumbs: [
            {
              thumb: 'images/thumbs/camping01.jpg',
              full: 'images/camping01.jpg'
            },
            {
              thumb: 'images/thumbs/camping02.jpg',
              full: 'images/camping02.jpg'
            },
            {
              thumb: 'images/thumbs/camping03.jpg',
              full: 'images/camping03.jpg'
            }
          ]
        },
      ],
      gallery3: [
        {
          title: 'Chamarras',
          thumbs: [
            {
              thumb: 'images/thumbs/chamarra01.jpg',
              full: 'images/chamarra01.jpg'
            },
            {
              thumb: 'images/thumbs/chamarra02.jpg',
              full: 'images/chamarra02.jpg'
            },
            {
              thumb: 'images/thumbs/chamarra03.jpg',
              full: 'images/chamarra03.jpg'
            }
          ]
        },
        {
          title: 'Pantalones',
          thumbs: [
            {
              thumb: 'images/thumbs/pantalon01.jpg',
              full: 'images/pantalon01.jpg'
            },
            {
              thumb: 'images/thumbs/pantalon02.jpg',
              full: 'images/pantalon02.jpg'
            },
            {
              thumb: 'images/thumbs/pantalon03.jpg',
              full: 'images/pantalon03.jpg'
            }
          ]
        },
        {
          title: 'Overoles',
          thumbs: [
            {
              thumb: 'images/thumbs/overol-confeccion01.jpg',
              full: 'images/overol-confeccion01.jpg'
            },
            {
              thumb: 'images/thumbs/overol-confeccion02.jpg',
              full: 'images/overol-confeccion02.jpg'
            },
            {
              thumb: 'images/thumbs/overol03.jpg',
              full: 'images/overol03.jpg'
            }
          ]
        },
        {
          title: 'Chalecos',
          thumbs: [
            {
              thumb: 'images/thumbs/chalecos01.jpg',
              full: 'images/chalecos02.jpg'
            },
            {
              thumb: 'images/thumbs/chalecos02.jpg',
              full: 'images/chalecos03.jpg'
            },
            {
              thumb: 'images/thumbs/chaleco-confeccion01.jpg',
              full: 'images/chaleco-confeccion01.jpg'
            }
          ]
        },
        {
          title: 'Poleras',
          thumbs: [
            {
              thumb: 'images/thumbs/polera01.jpg',
              full: 'images/polera01.jpg'
            },
            {
              thumb: 'images/thumbs/polera02.jpg',
              full: 'images/polera02.jpg'
            },
            {
              thumb: 'images/thumbs/polera03.jpg',
              full: 'images/polera03.jpg'
            }
          ]
        },
        {
          title: 'Gorras',
          thumbs: [
            {
              thumb: 'images/thumbs/gorras01.jpg',
              full: 'images/gorras01.jpg'
            },
            {
              thumb: 'images/thumbs/gorras02.jpg',
              full: 'images/gorras02.jpg'
            },
            {
              thumb: 'images/thumbs/gorras03.jpg',
              full: 'images/gorras03.jpg'
            }
          ]
        },
        {
          title: 'Mochilas',
          thumbs: [
            {
              thumb: 'images/thumbs/mochila01.jpg',
              full: 'images/mochila01.jpg'
            },
            {
              thumb: 'images/thumbs/mochila02.jpg',
              full: 'images/mochila02.jpg'
            },
            {
              thumb: 'images/thumbs/mochila03.jpg',
              full: 'images/mochila03.jpg'
            }
          ]
        },
        {
          title: 'Maletines',
          thumbs: [
            {
              thumb: 'images/thumbs/maletin01.jpg',
              full: 'images/maletin01.jpg'
            },
            {
              thumb: 'images/thumbs/maletin02.jpg',
              full: 'images/maletin02.jpg'
            },
            {
              thumb: 'images/thumbs/maletin03.jpg',
              full: 'images/maletin03.jpg'
            }
          ]
        },
        {
          title: 'Carpas',
          thumbs: [
            {
              thumb: 'images/thumbs/carpas01.jpg',
              full: 'images/carpas01.jpg'
            },
            {
              thumb: 'images/thumbs/carpas02.jpg',
              full: 'images/carpas02.jpg'
            },
            {
              thumb: 'images/thumbs/carpas03.jpg',
              full: 'images/carpas03.jpg'
            }
          ]
        },
        {
          title: 'Equipo de camping',
          thumbs: [
            {
              thumb: 'images/thumbs/camping01.jpg',
              full: 'images/camping01.jpg'
            },
            {
              thumb: 'images/thumbs/camping02.jpg',
              full: 'images/camping02.jpg'
            },
            {
              thumb: 'images/thumbs/camping03.jpg',
              full: 'images/camping03.jpg'
            }
          ]
        },
      ],
      gallery4: [
        {
          title: 'Tinglados',
          thumbs: [
            {
              thumb: 'images/thumbs/estructuras-metalicas01.jpg',
              full: 'images/estructuras-metalicas01.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas02.jpg',
              full: 'images/estructuras-metalicas02.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas12.jpg',
              full: 'images/estructuras-metalicas12.jpg'
            },            
            {
              thumb: 'images/thumbs/estructuras-metalicas13.jpg',
              full: 'images/estructuras-metalicas13.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas14.jpg',
              full: 'images/estructuras-metalicas14.jpg'
            },
            {
              thumb: 'images/no-disponible.jpg',
              full: 'images/no-disponible.jpg'
            }
          ]
        },
        {
          title: 'Cerchas',
          thumbs: [
            {
              thumb: 'images/thumbs/estructuras-metalicas04.jpg',
              full: 'images/estructuras-metalicas04.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas05.jpg',
              full: 'images/estructuras-metalicas05.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas06.jpg',
              full: 'images/estructuras-metalicas06.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas07.jpg',
              full: 'images/estructuras-metalicas07.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas08.jpg',
              full: 'images/estructuras-metalicas08.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas09.jpg',
              full: 'images/estructuras-metalicas09.jpg'
            }
          ]
        },
        {
          title: 'Cubiertas',
          thumbs: [
            {
              thumb: 'images/thumbs/estructuras-metalicas10.jpg',
              full: 'images/estructuras-metalicas10.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas11.jpg',
              full: 'images/estructuras-metalicas11.jpg'
            },
            {
              thumb: 'images/thumbs/estructuras-metalicas15.jpg',
              full: 'images/estructuras-metalicas15.jpg'
            }
          ]
        },
      ],
      gallery5: [
        {
          title: 'Dormitorios',
          thumbs: [
            {
              thumb: 'images/thumbs/campamentos01.jpg',
              full: 'images/campamentos01.jpg'
            },
            {
              thumb: 'images/thumbs/campamentos02.jpg',
              full: 'images/campamentos02.jpg'
            },
            {
              thumb: 'images/thumbs/campamentos03.jpg',
              full: 'images/campamentos03.jpg'
            }
          ]
        },
        {
          title: 'Oficinas',
          thumbs: [
            {
              thumb: 'images/thumbs/campamentos04.jpg',
              full: 'images/campamentos04.jpg'
            },
            {
              thumb: 'images/thumbs/campamentos05.jpg',
              full: 'images/campamentos05.jpg'
            },
            {
              thumb: 'images/thumbs/campamentos07.jpg',
              full: 'images/campamentos07.jpg'
            }
          ]
        },
        {
          title: 'Baños portátiles',
          thumbs: [
            {
              thumb: 'images/thumbs/campamentos06.jpg',
              full: 'images/campamentos06.jpg'
            },
            {
              thumb: 'images/thumbs/campamentos09.jpg',
              full: 'images/campamentos09.jpg'
            },
            {
              thumb: 'images/thumbs/campamentos10.jpg',
              full: 'images/campamentos10.jpg'
            },
          ]
        },
      ],
      gallery6: [
        {
          title: 'Letreros luminosos',
          thumbs: [
            {
              thumb: 'images/thumbs/luminoso01.jpg',
              full: 'images/luminoso01.jpg'
            },
            {
              thumb: 'images/thumbs/luminoso02.jpg',
              full: 'images/luminoso02.jpg'
            },
            {
              thumb: 'images/thumbs/luminoso03.jpg',
              full: 'images/luminoso03.jpg'
            }
          ]
        },
        {
          title: 'Banners',
          thumbs: [
            {
              thumb: 'images/thumbs/banner01.jpg',
              full: 'images/banner01.jpg'
            },
            {
              thumb: 'images/thumbs/banner02.jpg',
              full: 'images/banner02.jpg'
            },
            {
              thumb: 'images/thumbs/slide6.jpg',
              full: 'images/slider/slide6.jpg'
            }
          ]
        },
        {
          title: 'Vallas',
          thumbs: [
            {
              thumb: 'images/thumbs/vallas01.jpg',
              full: 'images/vallas01.jpg'
            },
            {
              thumb: 'images/thumbs/vallas02.jpg',
              full: 'images/vallas02.jpg'
            },
            {
              thumb: 'images/thumbs/vallas03.jpg',
              full: 'images/vallas03.jpg'
            }
          ]
        },
        {
          title: 'Adhesivos',
          thumbs: [
            {
              thumb: 'images/thumbs/adhesivo01.jpg',
              full: 'images/adhesivo01.jpg'
            },
            {
              thumb: 'images/thumbs/adhesivo02.jpg',
              full: 'images/adhesivo02.jpg'
            },
            {
              thumb: 'images/thumbs/adhesivo03.jpg',
              full: 'images/adhesivo03.jpg'
            }
          ]
        },
        {
          title: 'Microperforados',
          thumbs: [
            {
              thumb: 'images/thumbs/microperforado01.jpg',
              full: 'images/microperforado01.jpg'
            },
            {
              thumb: 'images/thumbs/microperforado02.jpg',
              full: 'images/microperforado02.jpg'
            },
            {
              thumb: 'images/thumbs/microperforado03.jpg',
              full: 'images/microperforado03.jpg'
            }
          ]
        },
      ],
      gallery7: [
        {
          title: 'Material publicitario',
          thumbs: [
            {
              thumb: 'images/thumbs/material-publicitario01.jpg',
              full: 'images/material-publicitario01.jpg'
            },
            {
              thumb: 'images/thumbs/material-publicitario02.jpg',
              full: 'images/material-publicitario02.jpg'
            },
            {
              thumb: 'images/thumbs/slide7.png',
              full: 'images/slider/slide7.png'
            }
          ]
        },
        {
          title: 'Estampados',
          thumbs: [
            {
              thumb: 'images/thumbs/estampados01.jpg',
              full: 'images/estampados01.jpg'
            },
            {
              thumb: 'images/thumbs/estampados02.jpg',
              full: 'images/estampados02.jpg'
            },
            {
              thumb: 'images/thumbs/estampados03.jpg',
              full: 'images/estampados03.jpg'
            }
          ]
        },
        {
          title: 'Bolsas',
          thumbs: [
            {
              thumb: 'images/thumbs/bolsas01.jpg',
              full: 'images/bolsas01.jpg'
            },
            {
              thumb: 'images/thumbs/bolsas02.jpg',
              full: 'images/bolsas02.jpg'
            },
            {
              thumb: 'images/thumbs/bolsas03.jpg',
              full: 'images/bolsas03.jpg'
            }
          ]
        },
      ],
      gallery8: [
        {
          title: 'Carpas para camping',
          thumbs: [
            {
              thumb: 'images/thumbs/carpas01.jpg',
              full: 'images/carpas01.jpg'
            },
            {
              thumb: 'images/thumbs/carpas02.jpg',
              full: 'images/carpas02.jpg'
            },
            {
              thumb: 'images/thumbs/carpas03.jpg',
              full: 'images/carpas03.jpg'
            },
            {
              thumb: 'images/thumbs/carpas01.jpg',
              full: 'images/carpas01.jpg'
            }
          ]
        },
        {
          title: 'Sombrillas',
          thumbs: [
            {
              thumb: 'images/thumbs/sombrilla01.jpg',
              full: 'images/sombrilla01.jpg'
            },
            {
              thumb: 'images/thumbs/sombrilla02.jpg',
              full: 'images/sombrilla02.jpg'
            },
            {
              thumb: 'images/thumbs/sombrilla01.jpg',
              full: 'images/sombrilla01.jpg'
            },
            {
              thumb: 'images/thumbs/sombrilla02.jpg',
              full: 'images/sombrilla02.jpg'
            }
          ]
        },
      ],
      gallery9: [
        {
          title: 'Articulos publicitarios',
          thumbs: [
            {
              thumb: 'images/thumbs/articulos-publicitarios02.jpg',
              full: 'images/articulos-publicitarios02.jpg'
            },
            {
              thumb: 'images/thumbs/articulos-publicitarios01.jpg',
              full: 'images/articulos-publicitarios01.jpg'
            },
            {
              thumb: 'images/thumbs/articulos-publicitarios02.jpg',
              full: 'images/articulos-publicitarios02.jpg'
            },
            {
              thumb: 'images/thumbs/articulos-publicitarios01.jpg',
              full: 'images/articulos-publicitarios01.jpg'
            }
          ]
        }
      ]
    },
    mounted: function () {
      var myLazyLoad = new LazyLoad()
      var header = new Headroom(document.querySelector('#header'), {
        tolerance: 15,
        offset: 180,
        tolerance: {
          up: 5,
          down: 0
        },
        classes: {
          initial: 'header-initial', //Cuando el elemento se inicializa
          pinned: 'header-up', //Cuando se deplaza hacia arriba
          unpinned: 'header-down', //Cuando se deplaza hacia abajo
          top: 'header-top', //Cuando esta pegado arriba
          notTop: 'header-notop', //Cuando no esta pegado arriba
          bottom: 'header-bottom', //Cuando esta pegado abajo
          notBottom: 'header-nobottom'//Cuando no esta pegado abajo
        }
      })
      header.init()
      let scroll = new SmoothScroll('a[href*="#"]',{
        offset: 180
      })
      baguetteBox.run('.article__content')
      let slider = tns({
        container: '.my-slider',
        controlsText:[
          '<',
          '>'
        ],
        autoplay: true
      })
    },
    created: function () {
      let wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: false,
        live: false
      })
      wow.init()
      const resize = () => {
        let sticky = new Sticky('.aside__content')
      }
      addEventListener('resize', resize)
      addEventListener('DOMContentLoaded', resize)
      //FOOTER AÑO
      function getDate(){
        var today = new Date()
        var year = today.getFullYear()
        document.getElementById('currentDate').innerHTML = year
      }      
      getDate()
    },
    methods: {
      showModal: function exitModal() {
        this.showmodal = false
      },
      sideMain: function sideMain(){
        if(this.menutemporal == true){
          this.menutemporal = false
          console.log('DESACTIVADO')
        }
        else{
          this.menutemporal = true
          console.log('ACTIVO')
        }
      },
      isFormValid: function () {
        return this.nombre != ''
      },
      submitForm: function () {
        if (!this.isFormValid()) return
        this.formSubmitted = true
        this.$http.post('mail.php', { vue: this.vue }).then(function (response) {
          // console.log(response)
          this.vue.envio = response.data
          this.clearForm()
        }, function () { })
      },
      clearForm: function () {
        this.vue.nombre = ''
        this.vue.email = ''
        this.vue.telefono = ''
        this.vue.movil = ''
        this.vue.direccion = ''
        this.vue.ciudad = ''
        this.vue.mensaje = ''
        this.vue.formSubmitted = false
      }
    }
  })
})()