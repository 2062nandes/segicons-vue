import gulp from 'gulp'
import sass from 'gulp-sass'
import pug from 'gulp-pug'
import plumber from 'gulp-plumber'
import postcss from 'gulp-postcss'
import autoprefixer from 'autoprefixer'
import cssnano from 'cssnano'
import pxtorem from 'postcss-pxtorem'
import browserSync from 'browser-sync'
import sourcemaps from 'gulp-sourcemaps'
import browserify from 'browserify'
import babel from 'gulp-babel'
import babelify from 'babelify'
import buffer from 'vinyl-buffer'
import source from 'vinyl-source-stream'

import concat from 'gulp-concat'
import rename from 'gulp-rename'
import uglify from 'gulp-uglify'

const server = browserSync.create();

let postcssPlugins = [
  autoprefixer({browsers: '> 1%, last 2 versions, Firefox ESR, Opera 12.1'}),
  cssnano({
    // core:true,
    discardComments: false,// se desactivó para manipular comentarios para sass
    core: false // se desactivó para manipular outputStyle para sass
  }),
  pxtorem
];

let sassOptions = {
  outputStyle: 'compact',
  sourceComments: false
};

gulp.task('styles', () =>
  gulp.src('./sass/styles.scss')
      .pipe(plumber({
        errorHandler: function (err) {
          console.log(err);
          this.emit('end');
        }
      }))
      .pipe(sass(sassOptions))
      .pipe(postcss(postcssPlugins))
      .pipe(plumber.stop())
      .pipe(gulp.dest('./public_html'))
      .pipe(server.stream())
);

gulp.task('compileCore', () =>
  gulp.src('./sass/ed-grid.scss')
    .pipe(sass(sassOptions))
    .pipe(postcss(postcssPlugins))
    .pipe(gulp.dest('./css'))
);

gulp.task('scripts', () =>
  browserify('./js/babel/menu.js', {
      debug: true,
      standalone: 'edgrid'
    })
    .transform(babelify)
    .bundle()
    .pipe(source('ed-grid.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./js'))
);

gulp.task('es6', () =>{
  gulp.src('es6/*.js')
  .pipe(babel())
  .pipe(gulp.dest('./public_html/js'))
});
         
gulp.task('vue', () =>{
  gulp.src([
    './node_modules/vue-resource/dist/vue-resource.min.js'
  ])
  .pipe(concat('concat.js'))
  .pipe(rename('vue-form.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./public_html/js'))
});

gulp.task('bundle', () => {
  gulp.src([
    './node_modules/vue/dist/vue.js',
    './node_modules/vanilla-lazyload/dist/lazyload.min.js',
    './node_modules/wow.js/dist/wow.min.js',
    './node_modules/headroom.js/dist/headroom.min.js',
    './node_modules/sticky-js/dist/sticky.min.js',
    './node_modules/smooth-scroll/dist/js/smooth-scroll.min.js',
    './node_modules/baguettebox.js/dist/baguetteBox.min.js',
    './node_modules/tiny-slider/dist/min/tiny-slider.js'
  ])
    .pipe(concat('concat.js'))
    .pipe(rename('bundle.js'))
    // .pipe(uglify())
    .pipe(gulp.dest('./public_html/js'))
});

gulp.task('sw', () =>
  gulp.watch('./**/**.scss', ['styles'])
);

gulp.task('pug', () =>
  gulp.src('./pug/**/*.pug')
    .pipe(plumber({
      errorHandler: function (err) {
        console.log(err);
        this.emit('end');
      }
    }))
    .pipe(pug({
      pretty:true
    }))
    .pipe(plumber.stop())
    .pipe(gulp.dest('./public_html'))
);

gulp.task('default', () => {
  server.init({
    server: {
      baseDir: './public_html' //Salida en la carpeta public_html
    },

    serveStatic: ['./js']
  });
  gulp.watch('./pug/**/**.pug',['pug']);
  gulp.watch('./**/**.scss', ['styles']);
  gulp.watch('./js/*.js', ['scripts']);
  gulp.watch('./es6/*.js',['es6']);
  gulp.watch('./es5/*.js', ['vue']);
  gulp.watch('./es5/*.js',['bundle']);
  gulp.watch('./public_html/**.html').on('change', server.reload);
  gulp.watch('./js/*.js').on('change', server.reload);
});
